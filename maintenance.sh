#!/bin/bash
echo
echo running partial package cleanup...

apt-get autoclean

echo
echo running apt cache clean...

apt-get clean

echo
echo cleaning dependencies...

apt-get autoremove

echo
echo finished...
